FROM openjdk:14-jdk-alpine

COPY ./gradle /gradle
COPY ./gradlew /
COPY ./*.gradle /

RUN ./gradlew build

CMD ./gradlew pactverify
